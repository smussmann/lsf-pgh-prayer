# LSF Pgh Prayer

This is an Appengine app that uses [Ordo][] to provide a simple daily prayer service on the web.

You can see it in action at https://lsf-pgh-prayer.appspot.com/ and http://prayer.lsfpgh.com/

[Ordo]: https://bitbucket.org/smussmann/ordo
