module bitbucket.org/smussmann/bible-pit-prayer

go 1.14

require (
	bitbucket.org/smussmann/ordo v0.0.0-20200325111541-6b70b4894a61
	github.com/gin-gonic/gin v1.6.1 // indirect
	github.com/golang/protobuf v1.3.5 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/vjeantet/eastertime v0.0.0-20160228130558-3941bbcf8209 // indirect
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a // indirect
	golang.org/x/sys v0.0.0-20200323222414-85ca7c5b95cd // indirect
	gopkg.in/go-playground/validator.v9 v9.31.0 // indirect
)
